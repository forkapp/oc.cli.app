<?php

	namespace App;

	class Model {

		/**
		 * @var \DB
		 */
		protected static $db;

		public static function setLink($link) {

			self::$db = $link;

		}

		public static function startTransaction() {

			self::$db->query('START TRANSACTION');

		}

		public static function endTransaction() {

			self::$db->query('COMMIT');

		}

	}
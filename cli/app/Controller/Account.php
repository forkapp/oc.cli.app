<?php

	namespace App\Controller;

	use App\App,
		App\Model;

	final class Account {

		/** @var \Log */
		protected static $log, $mail;

		private static $interval = 0;

		public static function initialize () {

			App::load([
				DIR_SYSTEM . 'library/log.php',
				DIR_STORAGE . 'vendor/autoload.php',
				DIR_SYSTEM . 'library/mail/mail.php',
				DIR_SYSTEM . 'library/mail.php',
			]);

			self::$mail = new \Mail();

			self::$mail->setFrom(App::getConfig('config_email'));
			self::$mail->parameter = App::getConfig('config_mail_parameter');
			self::$mail->setSender(
				html_entity_decode(App::getConfig('config_name'), ENT_QUOTES, 'UTF-8')
			);
			self::$mail->setSubject('Проверка активности аккаунтов: - ' . App::getConfig('config_name'));

			self::$log = new \Log('cli_account.log');

		}

		/**
		 * @throws \Exception
		 */
		public static function deactivate (): void {

			$customers = Model\Customer::getInActiveAccounts();
			$total = count($customers);

			foreach ($customers as $customer) {
				Model\Customer::setInActive((int)$customer[0]);
			}

			self::$mail->setTo(App::getConfig('config_email'));
			self::$mail->setText(strip_tags("Найдено $total неактивных акаунта"));

			try {
				self::$mail->send();
			} catch (\Exception $e) {
				self::$log->write($e);
			}

		}

	}
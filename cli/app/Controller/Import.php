<?php

	namespace App\Controller;

	require_once DIR_SYSTEM . 'helper/url_slug.php';
	require_once DIR_SYSTEM . 'library/log.php';

	use App\App,
		App\Helper,
		App\Imager,
		App\Model,
		App\Interfaces\Controller\Import as II;

	final class Import implements II {

		protected static $data_dir = '1c/';
		protected static $data_file = '1c/ProductToSite/full_export.json';

		/** @var \Log */
		protected static $log, $template, $mail, $customLog;

		private static $data = [
			'from_source' => [],
			'temp'        => [],
			'exists'      => [
				'uid'  => [],
				'name' => [],
			],
			'insert'      => [],
			'update'      => [],
		];

		public static function initialize () {

			self::$log = new \Log('import.log');

		}

		public static function getCollectData (string $prototype, string $collection_name, $key) {

			return self::$data['exists'][$collection_name][$prototype][$key] ?? null;

		}

		/**
		 * @throws \Exception
		 */
		public static function import () {

			self::$data['from_source'] = Helper::loadJson(DIR_STORAGE . self::$data_file);

			if (!self::$data['from_source']) {
				throw new \Exception('Data from 1C is empty!');
			}

			App::load(DIR_SYSTEM . 'helper/PHPTerminalProgressBar.php');

			self::prepareFromDb([
				'option'         => '\App\Model\Option',
				'option_value'   => '\App\Model\OptionValue',
				'category'       => '\App\Model\Category',
				'customer_group' => '\App\Model\CustomerGroup',
				'storage'        => '\App\Model\Storage',
				'manufacturer'   => '\App\Model\Manufacturer',
			]);

			foreach (Model\Product::getAll() as $item) {
				self::collectData(
					'product',
					'uid',
					[
						$item[1],
						(int)$item[0],
					]
				);

				self::collectData(
					'product',
					'name',
					[
						$item[2],
						(int)$item[0],
					]
				);

				self::collectData(
					'product',
					'pid_name',
					[
						(int)$item[0],
						$item[3],
					]
				);

				self::collectData(
					'product',
					'pid_cid',
					[
						(int)$item[0],
						$item[4],
					]
				);
			}

			self::executeProperties();
			self::executeCategories();
			self::executePriceGroups();
			self::executeStorages();
			self::executeProducts();

		}

		public static function deleteProducts () {

			$files = Helper::dirMap(DIR_STORAGE . self::$data_dir . 'ProductToSiteDelete/', false);

			foreach (array_keys($files) as $file) {
				self::deleteProduct($file);
			}

		}

		private static function deleteProduct ($file) {

			$item = Helper::loadJson($file);

			if ($item) {
				foreach ($item['deleteproduct'] as $item) {
					Model\Product::remove($item['id']);
					@unlink($file);
				}
			}

		}

		private static function prepareFromDb ($prototypes_to_prepare = []) {

			foreach ($prototypes_to_prepare as $prototype => $model) {

				foreach ($model::getAll() as $item) {
					self::collectData(
						$prototype,
						'uid',
						[
							$item[1],
							(int)$item[0],
						]
					);

					self::collectData(
						$prototype,
						'name',
						[
							$item[2],
							(int)$item[0],
						]
					);
				}
			}

		}

		public static function collectData (string $prototype, string $collection_name, array $data) {

			self::$data['exists'][$collection_name][$prototype][$data[0]] = $data[1];

		}

		private static function executeProperties () {

			if (isset(self::$data['from_source']['header']['property'])) {
				foreach (self::$data['from_source']['header']['property'] as $ik => $item) {

					$option_id = self::isExists('option', $item);

					$option = [
						'uid'        => $item['id'],
						'name'       => $item['value'],
						'sort_order' => $ik,
					];

					if (!$option_id) {

						$option_id = Model\Option::insert($option);

						self::collectData(
							'option',
							'uid',
							[
								$option['uid'],
								$option_id,
							]
						);

						self::collectData(
							'option',
							'name',
							[
								mb_strtolower($option['name']),
								$option_id,
							]
						);

						continue;

					}

					Model\Option::update(
						$option_id,
						$option
					);

				}

			}

		}

		private static function isExists ($prototype, &$object, $find_in_name = true) {

			return self::$data['exists']['uid'][$prototype][$object['id']]
				?? (
				$find_in_name
					? (
					self::$data['exists']['name'][$prototype][mb_strtolower(trim($object['value']))]
					?? null
				)
					: null
				);

		}

		private static function executeCategories (bool $disable_before = true) {

			if ($disable_before) {
				Model\Category::disable();
			}

			if (isset(self::$data['from_source']['header']['category'])) {
				self::executeCategory('');
			}

		}

		private static function executeCategory ($parent = 0) {

			$parents = [];

			foreach (self::$data['from_source']['header']['category'] as $ck => $category) {

				if ($category['parent'] != $parent)
					continue;

				$category_id = self::isExists('category', $category, false);

				$parent_id = self::get_parent_category_id($category['parent']);
				$path = Model\Category::getFullPath($parent_id);

				$data = [
					'name'       => trim($category['value']),
					'uid'        => trim($category['id']),
					'parent_id'  => $parent_id,
					'is_service' => isset($category['service']) ? (int)$category['service'] : 0,
					'path'       => $path,
				];

				$parents[$data['uid']] = null;

				if (!$category_id) {

					$category_id = Model\Category::insertCategory($data);

					self::collectData(
						'category',
						'uid',
						[
							$data['uid'],
							$category_id,
						]
					);

					self::collectData(
						'category',
						'name',
						[
							mb_strtolower($data['name']),
							$category_id,
						]
					);

					unset(self::$data['from_source']['header']['category'][$ck]);

					continue;

				}

				Model\Category::updateCategory(
					$category_id,
					$data
				);

				unset(self::$data['from_source']['header']['category'][$ck]);

			}

			foreach (array_keys($parents) as $parent) {
				self::executeCategory($parent);
			}

		}

		private static function get_parent_category_id (string $cid): int {

			return self::$data['exists']['uid']['category'][$cid] ?? 0;

		}

		private static function executePriceGroups (bool $disable_before = true) {

			if ($disable_before) {
				Model\CustomerGroup::disable();
			}

			if (isset(self::$data['from_source']['header']['price'])) {
				foreach (self::$data['from_source']['header']['price'] as $price_group) {

					$price_group_id = self::isExists('customer_group', $price_group);

					$data = [
						'name' => trim($price_group['value']),
						'uid'  => trim($price_group['id']),
					];

					if (!$price_group_id) {

						$price_group_id = Model\CustomerGroup::insert($data);

						self::collectData(
							'customer_group',
							'uid',
							[
								$data['uid'],
								$price_group_id,
							]
						);

						self::collectData(
							'customer_group',
							'name',
							[
								mb_strtolower($data['name']),
								$price_group_id,
							]
						);

						continue;

					}

					Model\CustomerGroup::update($price_group_id, $data);

				}
			}

			return;

		}

		private static function executeStorages (bool $disable_before = true) {

			if ($disable_before) {
				Model\Storage::disable();
			}

			if (isset(self::$data['from_source']['header']['storage'])) {
				foreach (self::$data['from_source']['header']['storage'] as $storage) {

					$storage_id = self::isExists('storage', $storage);

					$data = [
						'name' => trim($storage['value']),
						'uid'  => trim($storage['id']),
					];

					if (!$storage_id) {

						$storage_id = Model\Storage::insert($data);

						self::collectData(
							'storage',
							'uid',
							[
								$data['uid'],
								$storage_id,
							]
						);

						self::collectData(
							'storage',
							'name',
							[
								mb_strtolower($data['name']),
								$storage_id,
							]
						);

						continue;

					}

					Model\Storage::update($storage_id, $data);

				}
			}

		}

		private static function executeProducts (bool $disable_before = true) {

			if ($disable_before) {
				Model\Product::disable();
			}

			self::executeParentProducts();
			self::executeChildrenProducts();

		}

		private static function executeParentProducts () {

			if (isset(self::$data['from_source']['header']['parent'])) {

				$pb = new \PHPTerminalProgressBar(
					count(self::$data['from_source']['header']['parent']),
					"Processing parent products [:current/:total] :bar :percent%"
				);

				foreach (self::$data['from_source']['header']['parent'] as $product) {

					$data = [
						'uid'               => $product['id'],
						'name'              => $product['nameOnSite'],
						'parent_uid'        => null,
						'upc'               => $product['code'],
						'sku'               => null,
						'sale'              => 0,
						'service'           => 0,
						'archived'          => 0,
						'some_new'          => 0,
						'parent_product_id' => 0,
						'model'             => null,
						'quantity'          => 0,
						'manufacturer_id'   => 0,
						'price'             => 0,
						'prices'            => [],
						'storages'          => [],
						'attributes'        => [],
						'categories'        => [],
						'options'           => [],
						'brands'            => [],
						'models'            => [],
					];

					foreach (App::getLanguageIds() as $lid) {
						$data['descriptions'][$lid] = [
							'name'             => $product['nameOnSite'],
							'description'      => $product['textDescriprion'],
							'meta_title'       => $product['title'],
							'meta_description' => $product['description'],
							'meta_keyword'     => $product['findText'],
						];
					}

					foreach ($product['mCategory'] as $item) {

						$category_id = self::get_category_id_by_uid($item['value']);

						if ($category_id) {
							$data['categories'][] = $category_id;
						}

					}

					foreach ($product['brand'] as $item) {
						$data['brands'][] = trim($item['value']);
					}

					foreach ($product['model'] as $item) {
						$data['models'][] = trim($item['value']);
					}

					$product_id = self::isExists('product', $product);

					if (!$product_id) {

						$product_id = Model\Product::insert($data);

						self::collectData(
							'product',
							'uid',
							[
								$data['uid'],
								$product_id,
							]
						);

						self::collectData(
							'product',
							'name',
							[
								$data['name'],
								$product_id,
							]
						);

						self::collectData(
							'product',
							'pid_name',
							[
								$product_id,
								$data['name'],
							]
						);

						self::collectData(
							'product',
							'pid_cid',
							[
								$product_id,
								($data['categories'] ? implode(',', $data['categories']) : ''),
							]
						);

						continue;

					}

					Model\Product::update($product_id, $data);

					$pb->tick();

				}
			}

		}

		private static function get_category_id_by_uid (string $uid): int {

			return self::$data['exists']['uid']['category'][$uid] ?? 0;

		}

		private static function executeChildrenProducts () {

			if (isset(self::$data['from_source']['body']['product'])) {

				$pb = new \PHPTerminalProgressBar(
					$total = count(self::$data['from_source']['body']['product']),
					"Processing children products [:current/:total] :bar :percent%"
				);

				foreach (self::$data['from_source']['body']['product'] as $product) {

					$product_id = self::isExists('product', $product['mainProperty'], false);

					$data = [
						'uid'             => $product['mainProperty']['id'],
						'parent_uid'      => $product['mainProperty']['IDParent'],
						'sku'             => $product['mainProperty']['article'],
						'upc'             => (int)$product['mainProperty']['code'],
						'sale'            => (int)$product['mainProperty']['sale'],
						'some_new'        => (int)$product['mainProperty']['someNew'],
						'service'         => 0,
						'archived'        => 0,
						'model'           => null,
						'quantity'        => 0,
						'manufacturer_id' => 0,
						'price'           => 0,
						'prices'          => [],
						'storages'        => [],
						'attributes'      => [],
						'categories'      => [],
						'options'         => [],
						'brands'          => [],
						'models'          => [],
					];

					$data['parent_product_id']
						= self::$data['exists']['uid']['product'][$product['mainProperty']['IDParent']] ?? 0;

					$data['categories'] = explode(',',
						self::$data['exists']['pid_cid']['product'][$data['parent_product_id']] ?? ''
					);

					if (!$data['parent_product_id']) {
						self::$log->write('Product parent not found, UID: ' . $product['mainProperty']['id']);
						continue;
					}

					foreach ($product['price'] as $price) {
						$customer_group_id = self::isExists('customer_group', $price, false);
						$data['prices'][$customer_group_id] = Helper::clearFloat($price['value']);
					}

					foreach ($product['storage'] as $storage) {
						$storage_id = self::isExists('storage', $storage, false);
						$data['storages'][$storage_id] = (int)$storage['value'];
					}

					$name_suffix = '';

					foreach ($product['secondaryProperty'] as $item) {

						$option_id = self::isExists('option', $item);

						if (!$option_id) {
							continue;
						}

						$option_value_id = self::$data['exists']['name']['option_value'][md5($item['value'])] ?? 0;

						if (!$option_value_id) {

							$option_value_id = Model\OptionValue::insert([
								'name'      => $item['value'],
								'option_id' => $option_id,
							]);

							self::collectData(
								'option_value',
								'name',
								[
									md5($item['value']),
									$option_value_id,
								]
							);

							$data['options'][$option_id] = $option_value_id;

							$name_suffix .= ', ' . $item['value'];

							continue;

						}

						$data['options'][$option_id] = $option_value_id;

						$name_suffix .= ', ' . $item['value'];

					}

					foreach (App::getLanguageIds() as $lid) {
						$data['descriptions'][$lid] = [
							'name'             => self::$data['exists']['pid_name']['product'][$data['parent_product_id']]
								. ltrim($name_suffix, ','),
							'description'      => null,
							'meta_title'       => null,
							'meta_description' => null,
							'meta_keyword'     => null,
						];
					}

					if (!$product_id) {

						$product_id = Model\Product::insert($data);

						self::collectData(
							'product',
							'uid',
							[
								$data['uid'],
								$product_id,
							]
						);

						continue;

					}

					Model\Product::update($product_id, $data);

					$pb->tick();

				}
			}

		}

		public static function importOrders () {

			$files = Helper::dirMap(DIR_STORAGE . self::$data_dir . 'OrderToSite/');

			if ($files) {
				self::prepareFromDb([
					'order_status' => Model\OrderStatus::class,
					'product'      => Model\Product::class,
				]);
				foreach (Model\Order::getAll() as $item) {
					self::collectData(
						'order',
						'uid',
						[
							$item[1],
							(int)$item[0],
						]
					);

					self::collectData(
						'order',
						'name',
						[
							$item[2],
							(int)$item[0],
						]
					);

					self::collectData(
						'order',
						'customer',
						[
							(int)$item[0],
							(int)$item[3],
						]
					);
				}
			}

			foreach (array_keys($files) as $file) {
				self::executeOrder($file);
			}

		}

		private static function executeOrder ($file) {

			$order = Helper::loadJson($file);

			if (!$order) {
				self::$log->write('Order data is incorrect: ' . $file);

				return false;
			}

			$order['order_id'] = self::$data['exists']['uid']['order'][$order['id']]
				?? (self::$data['exists']['name']['order'][$order['siteId']] ?? null);

			if ($order['order_id']) {

				$customer_id = self::$data['exists']['customer']['order'][$order['order_id']] ?? 0;

				if ($customer_id) {
					Model\Customer::updateBalance($customer_id, Helper::clearFloat($order['balance']));
				}

				if (isset(self::$data['exists']['name']['order_status'][$order['status']])) {
					$order['order_status_id'] = self::$data['exists']['name']['order_status'][$order['status']];
				} else {

					$order['order_status_id'] = Model\OrderStatus::insert($order['status']);

					self::collectData(
						'order_status',
						'name',
						[
							$order['status'],
							$order['order_status_id'],
						]
					);

				}

				$order['order_products'] = [];

				if (isset($order['products'])) {

					$order['totals'] = [
						[
							'code'       => 'sub_total',
							'title'      => 'За товары',
							'value'      => 0,
							'sort_order' => 1,
						],
						[
							'code'       => 'shipping',
							'title'      => 'Доставка',
							'value'      => 0,
							'sort_order' => 2,
						],
						[
							'code'       => 'total',
							'title'      => 'Итого',
							'value'      => 0,
							'sort_order' => 3,
						],
					];

					foreach ($order['products'] as $order_product) {
						if (isset(self::$data['exists']['uid']['product'][$order_product['id']])) {

							$order['order_products'][] = [
								'product_id' => self::$data['exists']['uid']['product'][$order_product['id']],
								'name'       => array_flip(
									self::$data['exists']['name']['product']
								)[self::$data['exists']['uid']['product'][$order_product['id']]],
								'quantity'   => (int)$order_product['quantity'],
								'price'      => (float)($order_product['amount'] / $order_product['quantity']),
								'total'      => (float)$order_product['amount'],
							];

							$order['totals'][0]['value'] += (float)$order_product['amount'];

						}
					}

					$order['totals'][2]['value'] = ($order['totals'][0]['value'] + $order['totals'][1]['value']);

				}

				Model\Order::update($order['order_id'], $order);

				@unlink($file);

				return true;

			}

			self::$log->write('Order not found in DB #: ' . $order['id']);

			return false;

		}

		public static function importShipments () {

			$files = Helper::dirMap(DIR_STORAGE . self::$data_dir . 'ShipmentToSite/', false);

			App::load([
				DIR_SYSTEM . 'library/log.php',
				DIR_STORAGE . 'vendor/autoload.php',
				DIR_SYSTEM . 'library/template/twig.php',
				DIR_SYSTEM . 'library/mail/mail.php',
				DIR_SYSTEM . 'library/mail.php',
			]);

			self::$customLog = new \Log('shipments.log');

			self::$template = new \Template\Twig();
			self::$mail = new \Mail();

			self::$mail->setFrom(App::getConfig('config_email'));
			self::$mail->parameter = App::getConfig('config_mail_parameter');
			self::$mail->setSender(
				html_entity_decode(App::getConfig('config_name'), ENT_QUOTES, 'UTF-8')
			);
			self::$mail->setSubject('Статус заказа - ' . App::getConfig('config_name'));

			self::$data['template'] = Model\Email::get(9);
			self::$data['template'] = !self::$data['template'] ? '' : self::$data['template']['template'];

			if ($files) {
				self::prepareFromDb([
					'order'        => Model\Order::class,
					'shipment'     => Model\Shipment::class,
					'order_status' => Model\OrderStatus::class,
					'product'      => Model\Product::class,
				]);
			}

			foreach (array_keys($files) as $file) {
				self::executeShipment($file);
			}

		}

		private static function executeShipment ($file) {

			$shipment = Helper::loadJson($file);

			if (!$shipment) {
				self::$log->write('Shipment data is incorrect: ' . $file);

				return false;
			}

			$shipment['order_id'] = self::$data['exists']['uid']['order'][$shipment['parent']]
				?? (self::$data['exists']['name']['order'][$shipment['siteId']] ?? 0);

			$shipment['tracking_number'] = $shipment['delivery']['NP_TTN'] ?? '';

			$html = Helper::prepareTemplate(
				self::$data['template'],
				[
					'order_id'        => $shipment['order_id'],
					'status'          => $shipment['status'],
					'order_info_link' => HTTPS_SERVER . 'shipment',
				]
			);

			if (isset(self::$data['exists']['name']['order_status'][$shipment['status']])) {
				$shipment['order_status_id'] = self::$data['exists']['name']['order_status'][$shipment['status']];
			} else {

				$shipment['order_status_id'] = Model\OrderStatus::insert($shipment['status']);

				self::collectData(
					'order_status',
					'name',
					[
						$shipment['status'],
						$shipment['order_status_id'],
					]
				);

			}

			$shipment['customer_id'] = Model\Customer::getCustomerIdByEmail($shipment['client']['email']);

			if ($shipment['customer_id']) {
				Model\Customer::updateBalance(
					$shipment['customer_id'],
					Helper::clearFloat($shipment['client']['balance'])
				);
			}

			if ($shipment['order_id'] || $shipment['customer_id']) {

				$shipment['order_shipment_id'] = self::$data['exists']['uid']['shipment'][$shipment['id']] ?? null;

				$customer_email = $shipment['client']['email']
					? $shipment['client']['email']
					: Model\Shipment::getCustomerEmailByOrder($shipment['order_id']);

				if ($shipment['order_shipment_id']) {
					Model\Shipment::update($shipment['order_shipment_id'], $shipment);

					if ($customer_email && $html) {
						self::$mail->setTo($customer_email);
						self::$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));

						try {
							self::$mail->send();
						} catch (\Exception $e) {
							self::$customLog->write($e);
							self::$customLog->write($shipment);
						}
					}

					@unlink($file);

					return true;
				}

				$shipment['order_shipment_id'] = Model\Shipment::insert($shipment);

				if ($customer_email && $html) {
					self::$mail->setTo($customer_email);
					self::$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));

					try {
						self::$mail->send();
					} catch (\Exception $e) {
						self::$customLog->write($e);
						self::$customLog->write($shipment);
					}
				}

				self::collectData(
					'shipment',
					'name',
					[
						$shipment['order_shipment_id'],
						$shipment['order_shipment_id'],
					]
				);

				self::collectData(
					'shipment',
					'uid',
					[
						$shipment['id'],
						$shipment['order_shipment_id'],
					]
				);

				@unlink($file);

				return true;

			}

			self::$log->write('Order #' . $shipment['parent'] . ' for Shipment not found in DB #: ' . $shipment['id']);

			return false;

		}

		public static function importReturns () {

			$files = Helper::dirMap(DIR_STORAGE . self::$data_dir . 'ReturnToSite/', false);

			App::load([
				DIR_SYSTEM . 'library/log.php',
				DIR_STORAGE . 'vendor/autoload.php',
				DIR_SYSTEM . 'library/template/twig.php',
				DIR_SYSTEM . 'library/mail/mail.php',
				DIR_SYSTEM . 'library/mail.php',
			]);

			self::$customLog = new \Log('returns.log');

			self::$template = new \Template\Twig();
			self::$mail = new \Mail();

			self::$mail->setFrom(App::getConfig('config_email'));
			self::$mail->parameter = App::getConfig('config_mail_parameter');
			self::$mail->setSender(
				html_entity_decode(App::getConfig('config_name'), ENT_QUOTES, 'UTF-8')
			);
			self::$mail->setSubject('Статус заявки на возврат - ' . App::getConfig('config_name'));

			self::$data['template'] = Model\Email::get(11);
			self::$data['template'] = !self::$data['template'] ? '' : self::$data['template']['template'];

			if ($files) {
				self::prepareFromDb([
					'returns'       => Model\Returns::class,
					'shipments'     => Model\Shipment::class,
					'return_status' => Model\ReturnStatus::class,
					'return_reason' => Model\ReturnReason::class,
				]);
			}

			foreach (Model\Order::getCustomers() as $customer) {
				self::collectData(
					'customers_by_order_uid',
					'uid',
					[
						$customer[1],
						$customer[0],
					]
				);
			}

			foreach (array_keys($files) as $file) {
				self::executeReturn($file);
			}

		}

		private static function executeReturn ($file) {

			$return = Helper::loadJson($file);

			if (!$return) {
				self::$log->write('Return data is incorrect: ' . $file);

				return false;
			}

			$return['delete'] = strtolower($return['delete']) === 'true' ? 1 : 0;
			$return['return_id'] = self::$data['exists']['uid']['returns'][$return['id']] ?? null;
			$return['return_status_id'] = App::getConfig('config_return_new_status_id');
			$return['customer_id'] = Model\Customer::getCustomerIdByEmail($return['client']['email']);
			$return['customer'] = json_encode($return['client'], 256);

			if ($return['customer_id']) {
				Model\Customer::updateBalance(
					$return['customer_id'],
					Helper::clearFloat($return['client']['balance'])
				);
			}

			$comment = md5($return['comment']);

			if (isset(self::$data['exists']['name']['return_status'][$comment])) {
				$return['return_status_id'] = self::$data['exists']['name']['return_status'][$comment];
			} else {
				self::collectData(
					'return_status',
					'name',
					[
						$comment,
						$return['return_status_id'] = Model\ReturnStatus::insert($return['comment']),
					]
				);
			}

			$return['return_status_id'] = $return['delete'] === 1
				? App::getConfig('config_return_canceled_status_id')
				: $return['return_status_id'];

			$products = [];

			foreach ($return['products'] as $ui => $product) {

				$products[$product['parent']][$ui] = $product;

				if (isset(self::$data['exists']['name']['return_reason'][$product['comment']])) {
					$products[$product['parent']][$ui]['return_reason_id'] = self::$data['exists']['name']['return_reason'][$product['comment']];
				} else {

					if (!$product['comment']) {
						$products[$product['parent']][$ui]['return_reason_id'] = 0;
						continue;
					}

					$products[$product['parent']][$ui]['return_reason_id'] = Model\ReturnReason::insert($product['comment']);

					self::collectData(
						'return_reason',
						'name',
						[
							$product['comment'],
							$products[$product['parent']][$ui]['return_reason_id'],
						]
					);

				}

			}

			$html = Helper::prepareTemplate(
				self::$data['template'],
				[
					'return_id'   => $return['return_id'],
					'status'      => $return['comment'],
					'return_href' => HTTPS_SERVER . 'returns',
				]
			);

			foreach ($products as $shipment_uid => $product) {

				$return['products'] = $product;
				$return['shipment_uid'] = $shipment_uid;

				if ($return['return_id']) {

					Model\Returns::update($return['return_id'], $return);

					if (filter_var($return['client']['email'], FILTER_VALIDATE_EMAIL) && $html) {
						self::$mail->setTo($return['client']['email']);
						self::$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));

						try {
							self::$mail->send();
						} catch (\Exception $e) {
							self::$customLog->write($e);
							self::$customLog->write($return);
						}
					}

					continue;

				}

				self::collectData(
					'returns',
					'uid',
					[
						$return['id'],
						Model\Returns::insert($return),
					]
				);

			}

			return true;

		}

		public static function importCustomers () {

			$customers = Helper::loadJson(DIR_STORAGE . self::$data_dir . 'customers.json');

			if ($customers) {
				self::prepareFromDb([
					'customer' => '\App\Model\Customer',
				]);
			}

			self::executeCustomers($customers);

		}

		private static function executeCustomers (&$customers) {

			foreach ($customers as $customer) {

				if (!$customer['email']) {
					continue;
				}

				$customer = [
					'uid'     => $customer['id'],
					'new_1c'  => 0,
					'name'    => $customer['name'],
					'surname' => $customer['surname'],
					'email'   => $customer['email'],
					'phone'   => $customer['phone'],
					'balance' => Helper::clearFloat($customer['balance']),
				];

				if (isset(self::$data['exists']['uid']['customer'][$customer['email']])) {

					Model\Customer::update(
						self::$data['exists']['uid']['customer'][$customer['email']],
						$customer
					);

					continue;

				}

				$customer['new_1c'] = 1;

				Model\Customer::insert($customer);

			}

		}

		public static function syncCurrency () {

			self::prepareFromDb([
				'currency' => \App\Model\Currency::class,
			]);

			foreach (Helper::loadJson(DIR_STORAGE . self::$data_dir . '/Currencies/black.json') as $item) {

				$item = [
					'name'  => strtoupper(trim($item['name'])),
					'value' => (float)$item['value'],
				];

				if ($item['value'] !== 1) {

					if (isset(self::$data['exists']['name']['currency'][$item['name']])) {

						Model\Currency::update(
							self::$data['exists']['name']['currency'][$item['name']],
							$item
						);

						continue;

					}

					Model\Currency::insert($item);

				}

			}

		}

		/**
		 * DEPRECETED
		 */
		public static function makeSymbolicLinks () {

			$dir_1c = DIR_STORAGE . self::$data_dir;

			foreach ([
				DIR_IMAGE . '1c/product_images' => $dir_1c . 'product_images'
			] as $link => $target) {

				if (is_link($link) || !is_dir($target)) {
					continue;
				}

				$cmd = "ln -s $target $link";

				`$cmd`;

			};

		}

		public static function moveImages () {

			$image_dir_1c = DIR_STORAGE . self::$data_dir . 'product_images/';
			$image_dir = DIR_IMAGE . '1c/product_images/';

			if (glob("$image_dir_1c*", GLOB_ONLYDIR)) {

				$cmd = "cp -r $image_dir_1c* $image_dir && rm -R $image_dir_1c*";
				`$cmd`;

				foreach (Model\Product::getAll() as $product) {
					Imager::$products[$product[1]] = (int)$product[0];
				}

				Imager::execute($image_dir);

			}

		}

		public static function updateProducts () {

			$files = Helper::dirMap(DIR_STORAGE . self::$data_dir . 'ProductToSiteUpdate/', false);

			if ($files) {

				App::load(DIR_SYSTEM . 'helper/PHPTerminalProgressBar.php');

				self::prepareFromDb([
					'option'         => Model\Option::class,
					'option_value'   => Model\OptionValue::class,
					'category'       => Model\Category::class,
					'customer_group' => Model\CustomerGroup::class,
					'storage'        => Model\Storage::class,
					'manufacturer'   => Model\Manufacturer::class,
				]);

				foreach (Model\Product::getAll() as $item) {
					self::collectData(
						'product',
						'uid',
						[
							$item[1],
							(int)$item[0],
						]
					);

					self::collectData(
						'product',
						'name',
						[
							$item[2],
							(int)$item[0],
						]
					);

					self::collectData(
						'product',
						'pid_name',
						[
							(int)$item[0],
							$item[3],
						]
					);

					self::collectData(
						'product',
						'pid_cid',
						[
							(int)$item[0],
							$item[4],
						]
					);
				}

			}

			foreach (array_keys($files) as $file) {
				self::executeUpdateProduct($file);
			}

		}

		private static function executeUpdateProduct ($file) {

			self::$data['from_source'] = Helper::loadJson($file);

			if ($file) {
				self::executeProperties();
				self::executeCategories(false);
				self::executePriceGroups(false);
				self::executeStorages(false);
				self::executeProducts(false);
			}

		}

	}
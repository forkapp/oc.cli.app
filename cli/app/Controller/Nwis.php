<?php

	namespace App\Controller;

	use App\App;
	use App\Helper;
	use App\Model;
	use App\Profiler;

	final class nwis {

		protected static $template, $mail, $log;
		private static $data = [
			'exists' => [],
		];

		public static function initialize () {

			App::load([
				#DIR_SYSTEM . 'library/image.php',
				DIR_SYSTEM . 'library/log.php',
				DIR_STORAGE . 'vendor/autoload.php',
				DIR_SYSTEM . 'library/template/twig.php',
				DIR_SYSTEM . 'library/mail/mail.php',
				DIR_SYSTEM . 'library/mail.php',
			]);

			self::$log = new \Log('nwis.log');

			self::$template = new \Template\Twig();
			self::$mail = new \Mail();

			self::$mail->setFrom(App::getConfig('config_email'));
			self::$mail->parameter = App::getConfig('config_mail_parameter');
			self::$mail->setSender(
				html_entity_decode(App::getConfig('config_name'), ENT_QUOTES, 'UTF-8')
			);
			self::$mail->setSubject('Спешите приобрести, товары снова в наличии!');

			self::$data['template'] = Model\Email::get(6);
			self::$data['template'] = !self::$data['template'] ? '' : self::$data['template']['template'];

		}

		public static function check (): void {

			foreach (Model\Nwis::getAll() as $item) {

				if (!filter_var($item[0], 274)) {
					continue;
				}

				$template_data = ['products' => []];

				$products = explode(',', $item[2]);
				$products = array_flip($products);

				foreach (array_keys($products) as $product_id) {

					if (isset(self::$data['exists']['products'][$product_id])) {
						$template_data['products'][] = self::$data['exists']['products'][$product_id];
						continue;
					}

					$product_id = Model\Product::get($product_id);

					if (!$product_id) {
						continue;
					}

					$image = DIR_IMAGE . $product_id[3];
					$image = HTTPS_SERVER . 'image/' . (file_exists($image) ? $product_id[3] : 'placeholder.png');

					self::$data['exists']['products'][$product_id[0]] = [
						'image' => $image,
						'name'  => $product_id[2],
						'href'  => HTTPS_SERVER . $product_id[1],
					];

					$template_data['products'][] = self::$data['exists']['products'][$product_id[0]];

				}

				if ($template_data['products']) {

					self::$template->set('products', $template_data['products']);

					$html = Helper::prepareTemplate(
						self::$data['template'],
						[
							'firstname' => $item[3],
							'products'  => self::$template->render('default/template/mail/parts/products'),
						]
					);

					self::$mail->setTo($item[0]);
					self::$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));

					try {
						self::$mail->send();
						Model\Nwis::setNotified($item[5]);
					} catch (\Exception $e) {
						self::$log->write($e);
						self::$log->write($item);
					}

				}

			}

		}

	}
<?php

	namespace App\Controller;

	use App\Model;

	final class migrateNews {

		private static $data = [
			'exists'	=> []
		];

		public static function initialize() {}

		public static function migrate() {

			foreach (Model\News::getAll() as $item) {
				self::$data['exists'][$item[1]] = (int)$item[0];
			}

			#var_dump(self::$data['exists']);

			foreach (Model\News::getAllOld() as $item) {

				$news_id = self::$data['exists'][md5($item['title_russian'])] ?? null;

				if (!$news_id) {

					$description = str_replace('/var/upload', 'image/catalog/upload', $item['content_russian']);
					/*preg_match("/src=\"([^\"]*)\"/i", $description, $matches);

					if (isset($matches[1])) {
						$description = str_replace($matches[1], 'catalog/' . $matches[1], $description);
					}*/

					$news_id = Model\News::insert([
						'image'				=> ($item['image'] ? 'catalog/' . $item['image'] : ''),
						'date_added'		=> $item['create_date'],
						'status'			=> (int)$item['is_public'],
						'title'				=> $item['title_russian'],
						'description'		=> $description,
						'short_description'	=> $item['announce_russian'],
						'seo_h1'			=> $item['seo_h1_russian'],
						'seo_title'			=> $item['seo_title_russian'],
						'seo_description'	=> $item['seo_description_russian'],
						'seo_keywords'		=> $item['seo_keywords_russian'],
						'url'				=> $item['url_key'],
					]);

					continue;

				}

			}

		}

	}
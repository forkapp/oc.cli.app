<?php

	namespace App\Controller;

	use App\App;
	use App\Helper;
	use App\Model;
	use App\Profiler;
	use LisDev\Delivery\NovaPoshtaApi2;

	final class NovaPoshta {

		protected static $log, $np;

		public static function initialize () {

			App::load([
				DIR_SYSTEM . 'library/log.php',
				DIR_STORAGE . 'vendor/autoload.php',
				DIR_SYSTEM . 'library/novaposhta/NovaPoshtaApi2.php',
				DIR_SYSTEM . 'helper/PHPTerminalProgressBar.php',
			]);

			self::$log = new \Log('nova_poshta.log');

			self::$np = new NovaPoshtaApi2(App::getConfig('config_shipping_np_api_key'));

		}

		public static function sync () {

			self::executeAreas();
			self::executeCities();
			self::executeWarehouses();

			self::$log->write('NP updated');

		}

		private static function executeWarehouses (): void {

			$cities = Model\NovaPoshta::getCities();

			$pb = new \PHPTerminalProgressBar(
				count($cities),
				"Processing warehouses by city [:current/:total] :bar :percent%"
			);

			Model\NovaPoshta::disable('np_warehouses');

			foreach ($cities as $city) {

				$warehouses = self::$np->getWarehouses($city['ref']);

				if ($warehouses['success'] && $warehouses['data']) {

					$sql = 'INSERT INTO ' . DB_PREFIX . 'np_warehouses ';
					$sql .= '(site_key, description, description_ru, short_address, short_address_ru, phone, ';
					$sql .= 'type_of_warehouse, ref, number, city_ref, city_description, city_description_ru, ';
					$sql .= 'longitude, latitude, post_finance, bicycle_parking, payment_access, post_terminal, ';
					$sql .= 'international_shipping, total_max_weight_allowed, place_max_weight_allowed, reception, ';
					$sql .= 'delivery, schedule, data_added, `status`)';
					$sql .= ' VALUES ';

					foreach ($warehouses['data'] as $warehouse) {
						$sql .= '(';
						$sql .= $warehouse['SiteKey'] . ', ';
						$sql .= '\'' . addslashes($warehouse['Description']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['DescriptionRu']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['ShortAddress']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['ShortAddressRu']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['Phone']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['TypeOfWarehouse']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['Ref']) . '\', ';
						$sql .= $warehouse['Number'] . ', ';
						$sql .= '\'' . addslashes($warehouse['CityRef']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['CityDescription']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['CityDescriptionRu']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['Longitude']) . '\', ';
						$sql .= '\'' . addslashes($warehouse['Latitude']) . '\', ';
						$sql .= $warehouse['PostFinance'] . ', ';
						$sql .= $warehouse['BicycleParking'] . ', ';
						$sql .= $warehouse['PaymentAccess'] . ', ';
						$sql .= $warehouse['POSTerminal'] . ', ';
						$sql .= $warehouse['InternationalShipping'] . ', ';
						$sql .= $warehouse['TotalMaxWeightAllowed'] . ', ';
						$sql .= $warehouse['PlaceMaxWeightAllowed'] . ', ';
						$sql .= '\'' . addslashes(json_encode($warehouse['Reception'], JSON_UNESCAPED_UNICODE)) . '\', ';
						$sql .= '\'' . addslashes(json_encode($warehouse['Delivery'], JSON_UNESCAPED_UNICODE)) . '\', ';
						$sql .= '\'' . addslashes(json_encode($warehouse['Schedule'], JSON_UNESCAPED_UNICODE)) . '\', ';
						$sql .= 'NOW(), 1';
						$sql .= '),';
					}

					$sql = rtrim($sql, ',');
					$sql .= ' ON DUPLICATE KEY UPDATE description=VALUES(description), `status`=VALUES(`status`)';

					Model\NovaPoshta::RAWSQL($sql);

				}

				$pb->tick();

			}

		}

		private static function executeCities (): void {

			$sql = 'INSERT INTO ' . DB_PREFIX . 'np_cities ';
			$sql .= '(description, description_ru, ref, delivery1, delivery2, delivery3, delivery4, delivery5, delivery6, delivery7, ';
			$sql .= 'area, settlement_type, is_branch, prevent_entry_new_streets_user, conglomerates, city_id, ';
			$sql .= 'settlement_type_description, settlement_type_description_ru)';
			$sql .= ' VALUES ';

			foreach (self::getCities() as $city) {
				$sql .= '(';
				$sql .= '\'' . addslashes($city['Description']) . '\', ';
				$sql .= '\'' . addslashes($city['DescriptionRu']) . '\', ';
				$sql .= '\'' . $city['Ref'] . '\', ';
				$sql .= $city['Delivery1'] . ', ';
				$sql .= $city['Delivery2'] . ', ';
				$sql .= $city['Delivery3'] . ', ';
				$sql .= $city['Delivery4'] . ', ';
				$sql .= $city['Delivery5'] . ', ';
				$sql .= $city['Delivery6'] . ', ';
				$sql .= $city['Delivery7'] . ', ';
				$sql .= '\'' . $city['Area'] . '\', ';
				$sql .= '\'' . $city['SettlementType'] . '\', ';
				$sql .= $city['IsBranch'] . ', ';
				$sql .= '\'' . addslashes($city['PreventEntryNewStreetsUser']) . '\', ';
				$sql .= '\'' . addslashes(json_encode($city['Conglomerates'])) . '\', ';
				$sql .= $city['CityID'] . ', ';
				$sql .= '\'' . addslashes($city['SettlementTypeDescription']) . '\', ';
				$sql .= '\'' . addslashes($city['SettlementTypeDescriptionRu']) . '\' ';
				$sql .= '),';
			}

			$sql = rtrim($sql, ',');
			$sql .= ' ON DUPLICATE KEY UPDATE description=VALUES(description),';
			$sql .= ' description_ru=VALUES(description_ru),';
			$sql .= ' ref=VALUES(ref),';
			$sql .= ' city_id=VALUES(city_id),';
			$sql .= ' settlement_type_description=VALUES(settlement_type_description),';
			$sql .= ' settlement_type_description_ru=VALUES(settlement_type_description_ru)';

			Model\NovaPoshta::RAWSQL($sql);

		}

		private static function executeAreas (): void {

			$sql = 'INSERT INTO ' . DB_PREFIX . 'np_areas ';
			$sql .= '(description, ref, areas_center)';
			$sql .= ' VALUES ';

			foreach (self::getAreas() as $area) {
				$sql .= '(';
				$sql .= '\'' . addslashes($area['Description']) . '\', ';
				$sql .= '\'' . addslashes($area['Ref']) . '\', ';
				$sql .= '\'' . addslashes($area['AreasCenter']) . '\' ';
				$sql .= '),';
			}

			$sql = rtrim($sql, ',');
			$sql .= ' ON DUPLICATE KEY UPDATE description=VALUES(description),';
			$sql .= ' ref=VALUES(ref),';
			$sql .= ' areas_center=VALUES(areas_center)';

			Model\NovaPoshta::RAWSQL($sql);

		}

		/**
		 * @return array
		 */
		private static function getAreas (): array {

			$result = self::$np->getAreas();

			return $result['success'] && $result['data'] ? (array)$result['data'] : [];

		}

		/**
		 * @return array
		 */
		private static function getCities (): array {

			$result = self::$np->getCities();

			return $result['success'] && $result['data'] ? (array)$result['data'] : [];

		}

	}
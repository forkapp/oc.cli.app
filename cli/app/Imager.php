<?php

	namespace App;

	use App\Model;

	final class Imager {

		public static $products = [];

		/**
		 * @param string $path
		 */
		public static function execute (string &$path): void {

			foreach (glob($path . '*', GLOB_ONLYDIR) as $dir) {

				$images = self::getImages($dir);

				$dir = pathinfo($dir)['basename'];

				if (!$images) {
					continue;
				}

				foreach (self::prepareImages($images, $dir) as $product_id => $images) {

					if (count($images) === 1) {
						Model\Product::setMainImage($product_id, $images[0]);
						continue;
					}

					Model\Product::setMainImage($product_id, $images[0]);

					Model\Product::setImages($product_id, array_slice($images, 1));

				}

			}

		}

		/**
		 * @param array $images
		 *
		 * @return array
		 */
		private static function prepareImages (array &$images, string $dir): array {

			$result = [];

			foreach (array_keys($images) as $image) {

				$image = pathinfo($image);
				$uid = explode('_', $image['filename'])[0];
				$product_id = self::$products[$uid] ?? false;

				if (!$product_id) {
					continue;
				}

				$result[$product_id][] = "1c/product_images/$dir/" . $image['basename'];

			};

			return $result;

		}

		/**
		 * @param string $path
		 *
		 * @return array
		 */
		private static function getImages (string &$path): array {

			$files = [];

			foreach (glob("$path/*.{jpg,png,gif}", GLOB_BRACE) as $file) {
				$files[$file] = true;
			}

			ksort($files, SORT_NATURAL);

			return $files;

		}

	}
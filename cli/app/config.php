<?php

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	if (file_exists($oc_config = __DIR__ . '/../../../config.php')) {
		require_once $oc_config;
		if (!file_exists($db_driver = DIR_SYSTEM . 'library/db/mpdo.php')) {
			throw new \Exception('DB driver can\'t be loaded!');
		}
		require_once $db_driver;
		require_once __DIR__ . '/initializer.php';
	} else {
		throw new \Exception('OpenCart config file not found!');
	}

	$config = [
		'routes' => [
			'import'       => \App\Controller\Import::class,
			'news_migrate' => \App\Controller\migrateNews::class,
			'nwis'         => \App\Controller\nwis::class,
			'np'           => \App\Controller\NovaPoshta::class,
			'account'      => \App\Controller\Account::class,
		],
	];
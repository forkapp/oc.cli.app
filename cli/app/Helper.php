<?php

	namespace App;

	final class Helper {

		public static function loadJson ($file): array {

			return file_exists($file)
				? json_decode(file_get_contents($file), true)
				: [];

		}

		public static function clearFloat (&$str): float {

			return (float)filter_var(
				str_replace(',', '.', $str),
				FILTER_SANITIZE_NUMBER_FLOAT,
				FILTER_FLAG_ALLOW_FRACTION
			);

		}

		public static function dirMap (string $dir, bool $sort = true): array {

			$files = [];

			foreach (glob($dir . '*.json') as $file) {
				$files[$file] = null;
			}

			if ($sort) {
				ksort($files, SORT_NATURAL);
			}

			return $files;

		}

		public static function prepareTemplate (string $template, array $data = []): string {

			preg_match_all('/{(.*?)}/', $template, $matches);

			$replacement = [];

			if (isset($matches[1])) {
				foreach ($matches[1] as $token) {
					$replacement['{' . $token . '}'] = $data[$token] ?? null;
				}
			}

			return strtr($template, $replacement);

		}

		public static function isValidUID (string $uid): bool {

			return (int)preg_replace('/[^0-9]/', '', $uid) !== 0;

		}

	}
<?php

	namespace App\Interfaces\Controller;

	interface Import {

		public static function initialize();

		public static function collectData(string $prototype, string $collection_name, array $data);

		public static function getCollectData(string $prototype, string $collection_name, $key);

	}
<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class News extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					n.news_id `0`,
					MD5(nd.title) `1`
				FROM
					' . DB_PREFIX . 'news n
				INNER JOIN
					' . DB_PREFIX . 'news_description nd ON (n.news_id = nd.news_id)
				WHERE
					nd.language_id = ' . App::getConfig('config_language_id') . '
			')->rows;

		}

		/**
		 * @return array
		 */
		public static function getAllOld (): array {

			return self::$db->query('
				SELECT
					*
				FROM
					fe_articles
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'news
				SET
					image		= \'' . self::$db->escape($data['image']) . '\',
					date_added	= \'' . self::$db->escape($data['date_added']) . '\',
					`status`	= ' . $data['status'] . '
			');

			$id = self::$db->getLastId();

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'news_description
					SET
						news_id				= ' . $id . ',
						language_id			= ' . $lid . ',
						title				= \'' . self::$db->escape($data['title']) . '\',
						description			= \'' . self::$db->escape($data['description']) . '\',
						short_description	= \'' . self::$db->escape($data['short_description']) . '\',
						seo_h1				= \'' . self::$db->escape($data['seo_h1']) . '\',
						seo_title			= \'' . self::$db->escape($data['seo_title']) . '\',
						seo_description		= \'' . self::$db->escape($data['seo_description']) . '\',
						seo_keywords		= \'' . self::$db->escape($data['seo_keywords']) . '\'
				');
			}

			SeoUrl::make($id, $data['url'], 'news_static');

			return $id;

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			return 1;

		}

	}
<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Customer extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					customer_id `0`,
					email `1`,
					customer_id `2`
				FROM
					' . DB_PREFIX . 'customer
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'customer
				SET
					uid					= \'' . $data['uid'] . '\',
					new_1c				= ' . $data['new_1c'] . ',
					customer_group_id	= ' . App::getConfig('config_customer_group_id') . ',
					store_id			= 0,
					language_id			= ' . App::getConfig('config_language_id') . ',
					firstname			= \'' . self::$db->escape($data['name']) . '\',
					lastname			= \'' . self::$db->escape($data['surname']) . '\',
					email				= \'' . self::$db->escape($data['email']) . '\',
					telephone			= \'' . self::$db->escape($data['phone']) . '\',
					balance				= \'' . self::$db->escape($data['balance']) . '\',
					status				= 1,
					safe				= 1,
					date_added			= NOW()
			');

			return self::$db->getLastId();

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {


			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'customer
				SET
					uid					= \'' . $data['uid'] . '\',
					new_1c				= ' . $data['new_1c'] . ',
					language_id			= ' . App::getConfig('config_language_id') . ',
					customer_group_id	= ' . App::getConfig('config_customer_group_id') . ',
					firstname			= \'' . self::$db->escape($data['name']) . '\',
					lastname			= \'' . self::$db->escape($data['surname']) . '\',
					email				= \'' . self::$db->escape($data['email']) . '\',
					telephone			= \'' . self::$db->escape($data['phone']) . '\',
					balance				= \'' . self::$db->escape($data['balance']) . '\',
					status				= 1,
					safe				= 1
				WHERE
					customer_id = ' . $id . '
			');

			return $id;

		}

		/**
		 * @param int $id
		 * @param float $balance
		 *
		 * @return int
		 */
		public static function updateBalance (int $id, float $balance): int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'customer
				SET
					balance = \'' . self::$db->escape($balance) . '\'
				WHERE
					customer_id = ' . $id . '
			');

			return $id;

		}

		/**
		 * @param int $id
		 *
		 * @return string
		 */
		public static function getEmail (int $id): string {

			$result = self::$db->query('
				SELECT
					email
				FROM
					' . DB_PREFIX . 'customer
				WHERE
					customer_id = ' . $id
			)->row;

			return $result ? $result['email'] : '';

		}

		/**
		 * @param string $email
		 *
		 * @return int
		 */
		public static function getCustomerIdByEmail (string $email): int {

			$result = self::$db->query('
				SELECT
					customer_id
				FROM
					' . DB_PREFIX . 'customer
				WHERE
					email = \'' . self::$db->escape($email) . '\''
			)->row;

			return $result ? (int)$result['customer_id'] : 0;

		}

		/**
		 * @return array
		 */
		public static function getInActiveAccounts (): array {

			$inactive_datetime = (new \DateTime())
				->modify('-' . (int)App::getConfig('config_login_deactivate_interval') . ' day')
				->format('Y-m-d H:i:s');

			return self::$db->query("
				SELECT
					customer_id `0`
				FROM
					" . DB_PREFIX . "customer
				WHERE
					last_activity < '$inactive_datetime'
				AND
					last_activity IS NOT NULL
				AND
					last_activity <> '0000-00-00 00:00:00'
				AND
					in_active <> 1
			")->rows;

		}

		/**
		 * @param int $customer_id
		 */
		public static function setInActive (int $customer_id): void {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'customer
				SET
					in_active = 1
				WHERE
					customer_id = ' . $customer_id . '
			');

		}

	}
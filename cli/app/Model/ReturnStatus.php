<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class ReturnStatus extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					return_status_id `0`,
					0 `1`,
					MD5(`name`) `2`
				FROM
					' . DB_PREFIX . 'return_status
			')->rows;

		}

		/**
		 * @param string $name
		 *
		 * @return int
		 */
		public static function insert (string $name): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'return_status
				SET
					language_id	= ' . App::getConfig('config_language_id') . ',
					`name`		= \'' . self::$db->escape($name) . '\'
			');

			return self::$db->getLastId();

		}

	}
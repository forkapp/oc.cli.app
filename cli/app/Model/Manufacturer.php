<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Manufacturer extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					manufacturer_id `0`,
					0 `1`,
					LOWER(TRIM(`name`)) `2`
				FROM
					' . DB_PREFIX . 'manufacturer
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'manufacturer
				SET
					`name`= \'' . self::$db->escape($data['name']) . '\'
			');

			$id = (int)self::$db->getLastId();

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'manufacturer_to_store
				SET
					manufacturer_id	= ' . $id . ',
					store_id		= ' . App::getConfig('config_store_id')
			);

			SeoUrl::make($id, $data['name'], 'manufacturer', 'brand-');

			return $id;

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			SeoUrl::make($id, $data['name'], 'manufacturer', 'brand-');

			return $id;

		}

	}
<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class CustomerGroup extends Model {

		public static function getAll() {

			return self::$db->query('
				SELECT
					cg.customer_group_id `0`,
					cg.uid `1`,
					LOWER(TRIM(cgd.`name`)) `2`
				FROM
					' . DB_PREFIX . 'customer_group cg
				INNER JOIN
					' . DB_PREFIX . 'customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id)
				WHERE
					cgd.language_id = ' . App::getConfig('config_language_id') . '
			')->rows;

		}

		public static function insert(array $data) : int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'customer_group
				SET
					uid			= \'' . $data['uid'] . '\',
					status		= 0,
					`approval`	= 1
			');

			$cg_id = self::$db->getLastId();

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'customer_group_description
					SET
						customer_group_id	= ' . $cg_id . ',
						language_id			= ' . $lid . ',
						`name`				= \'' . self::$db->escape($data['name']) . '\'
				');
			}

			return $cg_id;

		}

		public static function update(int $id, array $data) : int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'customer_group
				SET
					uid = \'' . $data['uid'] . '\',
					status = 1
				WHERE
					customer_group_id = ' . $id
			);

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					UPDATE
						' . DB_PREFIX . 'customer_group_description
					SET
						`name` = \'' . self::$db->escape($data['name']) . '\'
					WHERE
						customer_group_id = ' . $id . '
					AND
						language_id = ' . $lid . '
				');
			}

			return $id;

		}

		public static function disable() {

			self::$db->query('UPDATE ' . DB_PREFIX . 'customer_group SET status = 0 WHERE uid <> \'\'');

		}

	}
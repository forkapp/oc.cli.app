<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Product extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					p.product_id `0`,
					p.uid `1`,
					LOWER(TRIM(pd.`name`)) `2`,
					pd.`name` `3`,
					(
						SELECT
							GROUP_CONCAT(p2c.category_id)
						FROM
							' . DB_PREFIX . 'product_to_category p2c
						WHERE
							p2c.product_id = p.product_id
						GROUP BY
							p2c.product_id
						) `4`
				FROM
					' . DB_PREFIX . 'product p
				INNER JOIN
					' . DB_PREFIX . 'product_description pd ON p.product_id = pd.product_id
				WHERE
					pd.language_id = ' . App::getConfig('config_language_id') . '
			')->rows;

		}

		public static function get (int $id): array {

			return self::$db->query('
				SELECT
					p.product_id `0`,
					p.upc `1`,
					pd.`name` `2`,
					p.image `3`
				FROM
					' . DB_PREFIX . 'product p
				INNER JOIN
					' . DB_PREFIX . 'product_description pd ON p.product_id = pd.product_id
				WHERE
					pd.language_id = ' . App::getConfig('config_language_id') . '
				AND
					p.product_id = ' . $id . '
			')->row;

		}

		public static function insert(array $data) : int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'product
				SET
					uid					= \'' . $data['uid'] . '\',
					parent_uid			= \'' . $data['parent_uid'] . '\',
					parent_product_id	= ' . $data['parent_product_id'] . ',
					upc					= \'' . $data['upc'] . '\',
					sale				= ' . $data['sale'] . ',
					service				= ' . $data['service'] . ',
					archived			= ' . $data['archived'] . ',
					some_new			= ' . $data['some_new'] . ',
					model				= \'' . self::$db->escape($data['model']) . '\',
					sku					= \'' . self::$db->escape($data['sku']) . '\',
					quantity			= ' . $data['quantity'] . ',
					manufacturer_id		= ' . $data['manufacturer_id'] . ',
					price				= ' . $data['price'] . ',
					`status`			= 1,
					date_added			= NOW()
			');

			$id = (int)self::$db->getLastId();

			foreach ($data['descriptions'] as $lid => $description) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_description
					SET
						product_id			= ' . $id . ',
						language_id			= ' . $lid . ',
						`name`				= \'' . self::$db->escape($description['name']) . '\',
						`description`		= \'' . self::$db->escape($description['description']) . '\',
						meta_title			= \'' . self::$db->escape($description['meta_title']) . '\',
						meta_description	= \'' . self::$db->escape($description['meta_description']) . '\',
						meta_keyword		= \'' . self::$db->escape($description['meta_keyword']) . '\'
				');
			}

			foreach ($data['categories'] as $cid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_to_category
					SET
						product_id	= ' . $id . ',
						category_id	= ' . $cid . '
				');
			}

			foreach ($data['brands'] as $brand) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_manufacturer
					SET
						product_id	= ' . $id . ',
						brand		= \'' . self::$db->escape($brand) . '\'
				');
			}

			foreach ($data['models'] as $model) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_model
					SET
						product_id	= ' . $id . ',
						`name`		= \'' . self::$db->escape($model) . '\'
				');
			}

			foreach ($data['attributes'] as $lid => $attributes) {
				foreach ($attributes as $attribute_id => $value) {
					self::$db->query('
						INSERT INTO
							' . DB_PREFIX . 'product_attribute
						SET
							product_id		= ' . $id . ',
							attribute_id	= ' . $attribute_id . ',
							language_id		= ' . $lid . ',
							`text`			= \'' . self::$db->escape($value) . '\'
					');
				}
			}

			foreach ($data['options'] as $option_id => $option_value_id) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_options
					SET
						product_id			= ' . $id . ',
						parent_product_id	= ' . $data['parent_product_id'] . ',
						option_id			= ' . $option_id . ',
						option_value_id		= ' . $option_value_id . '
				');
			}

			foreach ($data['prices'] as $customer_group_id => $price) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_prices
					SET
						product_id			= ' . $id . ',
						parent_product_id	= ' . $data['parent_product_id'] . ',
						customer_group_id	= ' . $customer_group_id . ',
						price				= ' . $price . '
				');
			}

			foreach ($data['storages'] as $storage_id => $quantity) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_to_storage
					SET
						product_id			= ' . $id . ',
						parent_product_id	= ' . $data['parent_product_id'] . ',
						storage_id			= ' . $storage_id . ',
						quantity			= ' . $quantity . '
				');
			}

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'product_to_store
				SET
					product_id	= ' . $id . ',
					store_id	= ' . (int)App::getConfig('config_store_id') . '
			');

			return $id;

		}

		public static function update(int $id, array $data) : int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'product
				SET
					uid					= \'' . $data['uid'] . '\',
					parent_uid			= \'' . $data['parent_uid'] . '\',
					parent_product_id	= ' . $data['parent_product_id'] . ',
					upc					= \'' . $data['upc'] . '\',
					sale				= ' . $data['sale'] . ',
					service				= ' . $data['service'] . ',
					archived			= ' . $data['archived'] . ',
					some_new			= ' . $data['some_new'] . ',
					model				= \'' . self::$db->escape($data['model']) . '\',
					sku					= \'' . self::$db->escape($data['sku']) . '\',
					quantity			= ' . $data['quantity'] . ',
					manufacturer_id		= ' . $data['manufacturer_id'] . ',
					price				= ' . $data['price'] . ',
					`status`			= 1,
					date_modified		= NOW()
				WHERE
					product_id			= ' . $id . '
			');

			foreach ($data['descriptions'] as $lid => $description) {
				self::$db->query('
					UPDATE
						' . DB_PREFIX . 'product_description
					SET
						`name`				= \'' . self::$db->escape($description['name']) . '\',
						`description`		= \'' . self::$db->escape($description['description']) . '\',
						meta_title			= \'' . self::$db->escape($description['meta_title']) . '\',
						meta_description	= \'' . self::$db->escape($description['meta_description']) . '\',
						meta_keyword		= \'' . self::$db->escape($description['meta_keyword']) . '\'
					WHERE
						product_id			= ' . $id . '
					AND
						language_id			= ' . $lid . '
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_to_category WHERE product_id = ' . $id);

			foreach ($data['categories'] as $cid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_to_category
					SET
						product_id	= ' . $id . ',
						category_id	= ' . $cid . '
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_manufacturer WHERE product_id = ' . $id);

			foreach ($data['brands'] as $brand) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_manufacturer
					SET
						product_id	= ' . $id . ',
						brand		= \'' . self::$db->escape($brand) . '\'
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_model WHERE product_id = ' . $id);

			foreach ($data['models'] as $model) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_model
					SET
						product_id	= ' . $id . ',
						`name`		= \'' . self::$db->escape($model) . '\'
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_to_store WHERE product_id = ' . $id);

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'product_to_store
				SET
					product_id	= ' . $id . ',
					store_id	= ' . (int)App::getConfig('config_store_id') . '
			');

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_attribute WHERE product_id = ' . $id);

			foreach ($data['attributes'] as $lid => $attributes) {
				foreach ($attributes as $attribute_id => $value) {
					self::$db->query('
						INSERT INTO
							' . DB_PREFIX . 'product_attribute
						SET
							product_id		= ' . $id . ',
							attribute_id	= ' . $attribute_id . ',
							language_id		= ' . $lid . ',
							`text`			= \'' . self::$db->escape($value) . '\'
					');
				}
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_options WHERE product_id = ' . $id);

			foreach ($data['options'] as $option_id => $option_value_id) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_options
					SET
						product_id			= ' . $id . ',
						parent_product_id	= ' . $data['parent_product_id'] . ',
						option_id			= ' . $option_id . ',
						option_value_id		= ' . $option_value_id . '
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_prices WHERE product_id = ' . $id);

			foreach ($data['prices'] as $customer_group_id => $price) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_prices
					SET
						product_id			= ' . $id . ',
						parent_product_id	= ' . $data['parent_product_id'] . ',
						customer_group_id	= ' . $customer_group_id . ',
						price				= ' . $price . '
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_to_storage WHERE product_id = ' . $id);

			foreach ($data['storages'] as $storage_id => $quantity) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_to_storage
					SET
						product_id			= ' . $id . ',
						parent_product_id	= ' . $data['parent_product_id'] . ',
						storage_id			= ' . $storage_id . ',
						quantity			= ' . $quantity . '
				');
			}

			return $id;

		}

		public static function remove (string $uid): void {

			$product_id = self::$db->query('
				SELECT
					product_id `0`
				FROM
					' . DB_PREFIX . 'product
				WHERE
					uid = \'' . self::$db->escape($uid) . '\'
			')->row;

			if ($product_id) {
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_description
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_to_category
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_manufacturer
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_model
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_attribute
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_options
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_prices
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_to_storage
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_to_store
					WHERE
						product_id = ' . $product_id[0] . '
				');
				self::$db->query('
					DELETE
					FROM
						' . DB_PREFIX . 'product_image
					WHERE
						product_id = ' . $product_id[0] . '
				');
			}

		}

		public static function setMainImage (int $product_id, string $image): void {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'product
				SET
					image = \'' . self::$db->escape($image) . '\'
				WHERE
					product_id = ' . $product_id
			);

		}

		public static function setImages (int $product_id, array $images): void {

			self::$db->query('
				DELETE FROM
					' . DB_PREFIX . 'product_image
				WHERE
					product_id = ' . $product_id
			);

			foreach ($images as $sort_order => $image) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_image
					SET
						image		= \'' . self::$db->escape($image) . '\',
						sort_order 	= ' . $sort_order . ',
						product_id	= ' . $product_id
				);
			}

		}

		public static function disable() {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'product
				SET
					status = 0
				WHERE
					uid <> \'\'
				OR
					parent_uid <> \'\'
			');

		}

		public static function truncate() {

			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_description');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_to_category');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_to_store');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_manufacturer');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_model');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_prices');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_attribute');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_image');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_to_storage');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_variants');
			self::$db->query('DELETE FROM ' . DB_PREFIX . 'seo_url WHERE `query` LIKE \'product_id=%\'');

		}

	}
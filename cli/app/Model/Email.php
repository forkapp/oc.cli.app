<?php

	namespace App\Model;

	use App\Model;
	use App\App;

	final class Email extends Model {

		/**
		 * @param int $id
		 *
		 * @return array
		 */
		public static function get (int $id): array {

			return self::$db->query('
				SELECT
					template
				FROM
					' . DB_PREFIX . 'email_template et
				INNER JOIN
					' . DB_PREFIX . 'email_template_description etd ON (et.id = etd.email_template_id)
				WHERE
					et.id = ' . $id . '
				AND
					etd.language_id = ' . App::getConfig('config_language_id') . '
			')->row;

		}

	}
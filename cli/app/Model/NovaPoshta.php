<?php

	namespace App\Model;

	use App\Model;

	final class NovaPoshta extends Model {

		/**
		 * @param string $sql
		 */
		public static function RAWSQL (string $sql): void {

			self::$db->query($sql);

		}

		/**
		 * @return array
		 */
		public static function getCities (): array {

			return

				self::$db->query('
					SELECT
						ref
					FROM
						' . DB_PREFIX . 'np_cities
				')->rows;

		}

		public static function disable (string $table): void {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . $table . '
				SET
					`status` = 0
			');

		}

	}
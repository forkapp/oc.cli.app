<?php

	namespace App\Model;

	use App\Model;

	final class Order extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					order_id `0`,
					uid `1`,
					order_id `2`,
					customer_id `3`
				FROM
					`' . DB_PREFIX . 'order`
			')->rows;

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			self::$db->query('
				UPDATE
					`' . DB_PREFIX . 'order`
				SET
					date_modified	= \'' . $data['date'] . '\',
					order_status_id	= ' . $data['order_status_id'] . ',
					comment			= \'' . self::$db->escape($data['comment']) . '\',
					uid				= \'' . $data['id'] . '\'
				WHERE
					order_id = ' . $id . '
			');

			if (!$data['order_products']) {
				return $id;
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'order_product WHERE order_id = ' . $id);

			foreach ($data['order_products'] as $product) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'order_product
					SET
						order_id	= ' . $id . ',
						product_id	= ' . $product['product_id'] . ',
						`name`		= \'' . self::$db->escape($product['name']) . '\',
						model		= \'\',
						reward		= 0,
						quantity	= ' . $product['quantity'] . ',
						price		= ' . self::$db->escape($product['price']) . ',
						total		= ' . self::$db->escape($product['total']) . '
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'order_total WHERE order_id = ' . $id);

			foreach ($data['totals'] as $total) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'order_total
					SET
						order_id	= ' . $id . ',
						code		= \'' . self::$db->escape($total['code']) . '\',
						title		= \'' . self::$db->escape($total['title']) . '\',
						`value`		= ' . $total['value'] . ',
						sort_order	= ' . $total['sort_order'] . '
				');
			}

			return $id;

		}

		/**
		 * @return array
		 */
		public static function getCustomers (): array {

			return self::$db->query('
				SELECT
					customer_id `0`,
					uid `1`
				FROM
					`' . DB_PREFIX . 'order`
				WHERE
					customer_id <> 0
			')->rows;

		}

	}
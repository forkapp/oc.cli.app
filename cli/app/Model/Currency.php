<?php

	namespace App\Model;

	use App\Model;

	final class Currency extends Model {

		/**
		 * @return mixed
		 */
		public static function getAll () {

			return self::$db->query('
				SELECT
					currency_id `0`,
					currency_id `1`,
					UPPER(code) `2`
				FROM
					' . DB_PREFIX . 'currency
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT IGNORE INTO
					' . DB_PREFIX . 'currency
				SET
					`value`			= \'' . $data['value'] . '\',
					title			= \'' . self::$db->escape($data['name']) . '\',
					code			= \'' . self::$db->escape($data['name']) . '\',
					decimal_place	= 2,
					`status`		= 1,
					date_modified	= NOW()
			');

			return self::$db->getLastId();

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'currency
				SET
					`value`			= \'' . $data['value'] . '\',
					title			= \'' . self::$db->escape($data['name']) . '\',
					code			= \'' . self::$db->escape($data['name']) . '\',
					decimal_place	= 2,
					`status`		= 1,
					date_modified	= NOW()
				WHERE
					currency_id = ' . $id
			);

			return $id;

		}

	}
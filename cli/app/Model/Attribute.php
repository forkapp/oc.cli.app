<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Attribute extends Model {

		public static function getAll() {

			return self::$db->query('
				SELECT
					a.attribute_id `0`,
					a.uid `1`,
					LOWER(TRIM(ad.name)) `2`
				FROM
					' . DB_PREFIX . 'attribute a
				INNER JOIN
					' . DB_PREFIX . 'attribute_description ad ON a.attribute_id = ad.attribute_id
				WHERE
					ad.language_id = ' . App::getConfig('config_language_id') . '
			')->rows;

		}

		public static function insertAttribute(array $data) : int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'attribute
				SET
					attribute_group_id	= ' . $data['attribute_group_id'] . ',
					`status`			= 1,
					uid					= \'' . $data['uid'] . '\'
			');

			$attribute_id = (int)self::$db->getLastId();

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'attribute_description
					SET
						attribute_id	= ' . $attribute_id . ',
						language_id		= ' . $lid . ',
						`name`			= \'' . self::$db->escape($data['name']) . '\'
				');
			}

			return $attribute_id;

		}

		public static function updateAttribute(int $id, array $data) : int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'attribute
				SET
					attribute_group_id	= ' . $data['attribute_group_id'] . ',
					`status`			= 1,
					uid					= \'' . $data['uid'] . '\'
				WHERE
					attribute_id = ' . $id . '
			');

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'attribute_description WHERE attribute_id = ' . $id);

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'attribute_description
					SET
						attribute_id	= ' . $id . ',
						language_id		= ' . $lid . ',
						`name`			= \'' . self::$db->escape($data['name']) . '\'
				');
			}

			return $id;

		}

		public static function prepareMainGroup() : int {

			$attribute_group_id = self::$db->query('
				SELECT
					attribute_group_id `0`
				FROM
					' . DB_PREFIX . 'attribute_group_description
				WHERE
					language_id = ' . App::getConfig('config_language_id') . '
				AND
					`name` = \'Характеристики\'
			')->row;

			$attribute_group_id = $attribute_group_id ? $attribute_group_id[0] : 0;

			if ($attribute_group_id) {
				return (int)$attribute_group_id;
			}

			return self::insertGroup(['name' => 'Характеристики']);

		}

		public static function insertGroup(array $data) : int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'attribute_group
				SET
					sort_order = 0
			');

			$attribute_group_id = (int)self::$db->getLastId();

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'attribute_group_description
				SET
					attribute_group_id	= ' . $attribute_group_id . ',
					language_id			= ' . App::getConfig('config_language_id') . ',
					`name`				= \'' . self::$db->escape($data['name']) . '\'
			');

			return $attribute_group_id;

		}

		public static function disable() {

			self::$db->query('UPDATE ' . DB_PREFIX . 'attribute SET status = 0 WHERE uid <> \'\'');

		}

	}
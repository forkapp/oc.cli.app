<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Config extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					`key`,
					`value`,
					`serialized`
				FROM
					`' . DB_PREFIX . 'setting`
				WHERE
					store_id = ' . App::getConfig('config_store_id') . '
				AND
					`code`	= \'config\'
			')->rows;

		}

	}
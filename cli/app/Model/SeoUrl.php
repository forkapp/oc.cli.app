<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class SeoUrl extends Model {

		public static function make(string $id, string $name, string $prototype, string $prefix = '') : bool {

			$query = $push = $keyword = null;

			switch ($prototype) {
				case 'category':
					$push		= 'route=product/category&path=' . $id;
					$query		= 'path=' . $id;
					$keyword	= $prefix . url_slug($name);
					break;
				case 'product':
					$push		= 'route=product/product&product_id=' . $id;
					$query		= 'product_id=' . $id;
					$keyword	= $prefix . url_slug($name);
					break;
				case 'information':
					$push		= 'route=information/information&information_id=' . $id;
					$query		= 'information_id=' . $id;
					$keyword	= $prefix . url_slug($name);
					break;
				case 'manufacturer':
					$push		= 'route=product/manufacturer/info&manufacturer_id=' . $id;
					$query		= 'manufacturer_id=' . $id;
					$keyword	= $prefix . url_slug($name);
					break;
				case 'news_static':
					$push		= 'route=extension/news/news&news_id=' . $id;
					$query		= 'news_id=' . $id;
					$keyword	= $name;
					break;
				default:
					break;
			}

			if (!$query) {
				return false;
			}

			self::$db->query('
				DELETE FROM
					' . DB_PREFIX . 'seo_url
				WHERE
					`query` = \'' . $query . '\''
			);

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'seo_url
				SET
					store_id	= ' . (int)App::getConfig('config_store_id') . ',
					language_id	= ' . App::getConfig('config_language_id') . ',
					`query`		= \'' . $query . '\',
					`keyword`	= \'' . self::$db->escape($keyword) . '\',
					`push`		= \'' . $push . '\'
			');

			return true;

		}

		public static function resetTable (): void {

			self::$db->query('SET @num := 0');
			self::$db->query('UPDATE ' . DB_PREFIX. 'seo_url SET seo_url_id = @num := (@num + 1)');
			self::$db->query('ALTER TABLE ' . DB_PREFIX. 'seo_url AUTO_INCREMENT = 1');

		}

	}
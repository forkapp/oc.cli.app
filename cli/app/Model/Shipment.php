<?php

	namespace App\Model;

	use App\Model;

	final class Shipment extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					order_shipment_id `0`,
					uid `1`,
					order_shipment_id `2`
				FROM
					' . DB_PREFIX . 'order_shipment
			')->rows;

		}

		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'order_shipment
				SET
					order_id		= ' . $data['order_id'] . ',
					customer_id		= ' . $data['customer_id'] . ',
					uid				= \'' . self::$db->escape($data['id']) . '\',
					date_added		= \'' . $data['date'] . '\',
					tracking_number	= \'' . $data['tracking_number'] . '\',
					delivery		= \'' . self::$db->escape(json_encode($data['delivery'], 256)) . '\',
					comment			= \'' . self::$db->escape($data['comment']) . '\',
					order_status_id	= ' . $data['order_status_id'] . ',
					products		= \'' . self::$db->escape(json_encode($data['products'], 256)) . '\'
			');

			return self::$db->getLastId();

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'order_shipment
				SET
					order_id		= ' . $data['order_id'] . ',
					customer_id		= ' . $data['customer_id'] . ',
					uid				= \'' . self::$db->escape($data['id']) . '\',
					date_added		= \'' . $data['date'] . '\',
					tracking_number	= \'' . $data['tracking_number'] . '\',
					delivery		= \'' . self::$db->escape(json_encode($data['delivery'], 256)) . '\',
					comment			= \'' . self::$db->escape($data['comment']) . '\',
					order_status_id	= ' . $data['order_status_id'] . ',
					products		= \'' . self::$db->escape(json_encode($data['products'], 256)) . '\'
				WHERE
					order_shipment_id = ' . $id . '
			');

			return $id;

		}

		/**
		 * @param int $id
		 *
		 * @return string
		 */
		public static function getCustomerEmailByOrder (int $id): string {

			$result = self::$db->query('
				SELECT
					c.email
				FROM
					' . DB_PREFIX . 'customer c
				INNER JOIN
					`' . DB_PREFIX . 'order` o ON (c.customer_id = o.customer_id)
				WHERE
					o.order_id = ' . $id
			)->row;

			return $result ? $result['email'] : '';

		}

	}
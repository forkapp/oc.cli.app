<?php

	namespace App\Model;

	use App\Model;
	use App\Profiler;

	final class Nwis extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					nwis.email `0`,
					nwis.customer_id `1`,
					GROUP_CONCAT(nwis.product_id) `2`,
					c.firstname `3`,
					nwis.id `4`,
					GROUP_CONCAT(nwis.id) `5`
				FROM
					' . DB_PREFIX . 'notify_when_in_stock nwis
				LEFT JOIN
					' . DB_PREFIX . 'customer c ON (c.customer_id = nwis.customer_id)
				LEFT JOIN
					' . DB_PREFIX . 'product_to_storage p2s ON (nwis.product_id = p2s.product_id)
				WHERE
					nwis.notified = 0
				AND
					p2s.quantity > 0
				GROUP BY nwis.email
			')->rows;

		}

		/**
		 * @param string $ids
		 */
		public static function setNotified (string $ids): void {

			self::$db->query(
				'UPDATE ' . DB_PREFIX . 'notify_when_in_stock SET notified = 1 WHERE id IN(' . $ids . ')'
			);

		}

	}
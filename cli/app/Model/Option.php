<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Option extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					o.option_id `0`,
					o.uid `1`,
					LOWER(od.name) `2`
				FROM
					`' . DB_PREFIX . 'option` o
				INNER JOIN
					' . DB_PREFIX . 'option_description od ON (o.option_id = od.option_id)
				WHERE
					od.language_id = ' . App::getConfig('config_language_id') . '
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					`' . DB_PREFIX . 'option`
				SET
					uid			= \'' . $data['uid'] . '\',
					`type`		= \'select\',
					sort_order	= ' . $data['sort_order'] . '
			');

			$id = self::$db->getLastId();

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'option_description
				SET
					option_id	= ' . $id . ',
					language_id	= ' . App::getConfig('config_language_id') . ',
					`name`		= \'' . $data['name'] . '\'
			');

			return $id;

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			self::$db->query('
				UPDATE
					`' . DB_PREFIX . 'option`
				SET
					`type`		= \'select\',
					sort_order	= ' . $data['sort_order'] . '
				WHERE
					option_id	= ' . $id . '
			');

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'option_description WHERE option_id = ' . $id);

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'option_description
				SET
					option_id	= ' . $id . ',
					language_id	= ' . App::getConfig('config_language_id') . ',
					`name`		= \'' . $data['name'] . '\'
			');

			return $id;

		}

		public static function truncate (): void {

			self::$db->query('TRUNCATE TABLE `' . DB_PREFIX . 'option`');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'option_description');

		}

	}
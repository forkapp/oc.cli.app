<?php

	namespace App\Model;

	use App\Model;

	final class Storage extends Model {

		/**
		 * @return mixed
		 */
		public static function getAll () {

			return self::$db->query('
				SELECT
					id `0`,
					uid `1`,
					LOWER(TRIM(`name`)) `2`
				FROM
					' . DB_PREFIX . 'product_storages
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT IGNORE INTO
					' . DB_PREFIX . 'product_storages
				SET
					uid				= \'' . $data['uid'] . '\',
					`name`			= \'' . self::$db->escape($data['name']) . '\',
					`status`		= 1,
					date_created	= NOW()
			');

			return self::$db->getLastId();

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'product_storages
				SET
					uid				= \'' . $data['uid'] . '\',
					`name`			= \'' . self::$db->escape($data['name']) . '\',
					`status`		= 1,
					date_edited		= NOW()
				WHERE
					id = ' . $id
			);

			return $id;

		}

		public static function disable (): void {

			self::$db->query('UPDATE ' . DB_PREFIX . 'product_storages SET status = 0');

		}

	}
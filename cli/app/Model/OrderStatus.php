<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class OrderStatus extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					order_status_id `0`,
					0 `1`,
					`name` `2`
				FROM
					' . DB_PREFIX . 'order_status
			')->rows;

		}

		/**
		 * @param string $name
		 *
		 * @return int
		 */
		public static function insert (string $name): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'order_status
				SET
					language_id	= ' . App::getConfig('config_language_id') . ',
					`name`		= \'' . self::$db->escape($name) . '\'
			');

			return self::$db->getLastId();

		}

	}
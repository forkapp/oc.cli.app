<?php

	namespace App\Model;

	use App\Model;

	final class ProductVariants extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					pv.id `0`,
					pv.uid `1`,
					pv.id `2`
				FROM
					' . DB_PREFIX . 'product_variants pv
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'product_variants
				SET
					uid				= \'' . $data['uid'] . '\',
					parent_uid		= \'' . $data['parent_uid'] . '\',
					product_id		= ' . $data['product_id'] . ',
					sale			= ' . $data['sale'] . ',
					some_new		= ' . $data['some_new'] . ',
					code			= ' . $data['code'] . ',
					service			= ' . $data['service'] . ',
					sku				= \'' . self::$db->escape($data['sku']) . '\',
					`status`		= 1,
					date_created	= NOW()
			');

			$id = self::$db->getLastId();

			foreach ($data['options'] as $option_id => $option_value_id) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_variants_options
					SET
						variant_id		= ' . $id . ',
						option_id		= ' . $option_id . ',
						option_value_id	= ' . $option_value_id . '
				');
			}

			foreach ($data['prices'] as $customer_group_id => $price) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_prices
					SET
						variant_id			= ' . $id . ',
						customer_group_id	= ' . $customer_group_id . ',
						price				= ' . $price . '
				');
			}

			foreach ($data['storages'] as $storage_id => $quantity) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_to_storage
					SET
						variant_id	= ' . $id . ',
						storage_id	= ' . $storage_id . ',
						quantity	= ' . $quantity . '
				');
			}

			return $id;

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'product_variants
				SET
					parent_uid		= \'' . $data['parent_uid'] . '\',
					product_id		= ' . $data['product_id'] . ',
					sale			= ' . $data['sale'] . ',
					some_new		= ' . $data['some_new'] . ',
					code			= ' . $data['code'] . ',
					service			= ' . $data['service'] . ',
					sku				= \'' . self::$db->escape($data['sku']) . '\',
					`status`		= 1,
					date_edited		= NOW()
				WHERE
					id				= ' . $id . '
			');

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_variants_options WHERE variant_id = ' . $id);

			foreach ($data['options'] as $option_id => $option_value_id) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_variants_options
					SET
						variant_id		= ' . $id . ',
						option_id		= ' . $option_id . ',
						option_value_id	= ' . $option_value_id . '
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_prices WHERE id = ' . $id);

			foreach ($data['prices'] as $customer_group_id => $price) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_prices
					SET
						variant_id			= ' . $id . ',
						customer_group_id	= ' . $customer_group_id . ',
						price				= ' . $price . '
				');
			}

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'product_to_storage WHERE id = ' . $id);

			foreach ($data['storages'] as $storage_id => $quantity) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'product_to_storage
					SET
						variant_id	= ' . $id . ',
						storage_id	= ' . $storage_id . ',
						quantity	= ' . $quantity . '
				');
			}

			return $id;

		}

		public static function disable () {

			self::$db->query('UPDATE ' . DB_PREFIX . 'product_variants SET status = 0 WHERE uid <> \'\'');

		}

		public static function truncate () {

			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_variants');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'product_variants_options');

		}

	}
<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class OrderProduct extends Model {

		public static function getAll() {

			return [];

			return self::$db->query('
				SELECT
					p.product_id `0`,
					p.uid `1`,
					0 `2`
				FROM
					' . DB_PREFIX . 'order_product op
				INNER JOIN
					' . DB_PREFIX . 'product p ON (p.product_id = op.product_id)
			')->rows;

		}

		public static function insert(array $data) : int {

			return 0;

		}

		public static function update(int $id, array $data) : int {

			self::$db->query('
				UPDATE
					`' . DB_PREFIX . 'order`
				SET
					date_modified	= \'' . $data['date'] . '\',
					`status`		= 1,
					uid				= \'' . $data['id'] . '\'
				WHERE
					order_id = ' . $id . '
			');

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'attribute_description WHERE attribute_id = ' . $id);

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'attribute_description
					SET
						attribute_id	= ' . $id . ',
						language_id		= ' . $lid . ',
						`name`			= \'' . self::$db->escape($data['name']) . '\'
				');
			}

			return $id;

		}

	}
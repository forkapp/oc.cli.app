<?php

	namespace App\Model;

	use App\Model;

	final class Language extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					`language_id`,
					`name`,
					`code`
				FROM
					`' . DB_PREFIX . 'language`
				WHERE
					`status` = 1
			')->rows;

		}

	}
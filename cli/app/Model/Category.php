<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Category extends Model {

		public static function getAll() {

			return self::$db->query('
				SELECT
					c.category_id `0`,
					c.uid `1`,
					LOWER(TRIM(cd.name)) `2`
				FROM
					' . DB_PREFIX . 'category c
				INNER JOIN
					' . DB_PREFIX . 'category_description cd ON c.category_id = cd.category_id
				WHERE
					cd.language_id = ' . App::getConfig('config_language_id') . '
			')->rows;

		}

		public static function insertCategory(array $data) : int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'category
				SET
					parent_id	= ' . $data['parent_id'] . ',
					is_service	= ' . $data['is_service'] . ',
					`status`	= 1,
					date_added	= NOW(),
					uid			= \'' . $data['uid'] . '\'
			');

			$category_id = (int)self::$db->getLastId();

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					INSERT INTO
						' . DB_PREFIX . 'category_description
					SET
						category_id			= ' . $category_id . ',
						language_id			= ' . $lid . ',
						`name`				= \'' . self::$db->escape($data['name']) . '\',
						meta_title			= \'' . self::$db->escape($data['name']) . '\',
						meta_description	= \'' . self::$db->escape($data['name']) . '\'
				');
			}

			$level = 0;

			$raw_sql = '';

			$data['path'][] = $category_id;

			foreach ($data['path'] as $id) {
				$raw_sql .= '(' . $category_id . ', ' . $id . ', ' . $level . '),';
				$level++;
			}

			$raw_sql = rtrim($raw_sql, ',');

			self::$db->query(
				'INSERT INTO ' . DB_PREFIX . 'category_path (category_id, path_id, `level`) VALUES ' . $raw_sql
			);

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'category_to_store
				SET
					category_id	= ' . $category_id . ',
					store_id	= ' . (int)App::getConfig('config_store_id') . '
			');

			SeoUrl::make(implode('_', $data['path']), $data['name'], 'category');

			return $category_id;

		}

		public static function updateCategory(int $category_id, array $data) : int {

			self::$db->query('
				UPDATE
					' . DB_PREFIX . 'category
				SET
					parent_id		= ' . $data['parent_id'] . ',
					is_service		= ' . $data['is_service'] . ',
					`status`		= 1,
					date_modified	= NOW(),
					uid				= \'' . $data['uid'] . '\'
				WHERE
					category_id		= ' . $category_id
			);

			foreach (App::getLanguageIds() as $lid) {
				self::$db->query('
					UPDATE
						' . DB_PREFIX . 'category_description
					SET
						`name`				= \'' . self::$db->escape($data['name']) . '\',
						meta_title			= \'' . self::$db->escape($data['name']) . '\',
						meta_description	= \'' . self::$db->escape($data['name']) . '\'
					WHERE
						category_id = ' . $category_id . '
					AND
						language_id = ' . $lid . '
				');
			}

			$level = 0;

			$raw_sql = '';

			$data['path'][] = $category_id;

			foreach ($data['path'] as $id) {
				$raw_sql .= '(' . $category_id . ', ' . $id . ', ' . $level . '),';
				$level++;
			}

			$raw_sql = rtrim($raw_sql, ',');

			self::$db->query('DELETE FROM ' . DB_PREFIX . 'category_path WHERE category_id = ' . $category_id);

			self::$db->query(
				'INSERT INTO ' . DB_PREFIX . 'category_path (category_id, path_id, `level`) VALUES ' . $raw_sql
			);

			SeoUrl::make(implode('_', $data['path']), $data['name'], 'category');

			return $category_id;

		}

		public static function getFullPath(int $category_id) : array {

			$response = [];

			$result = self::$db->query('
				SELECT
					c.category_id `0`
				FROM
					' . DB_PREFIX . 'category_path cp
				INNER JOIN
					' . DB_PREFIX . 'category c ON (c.category_id = cp.path_id)
				WHERE
					cp.category_id = ' . $category_id
			)->rows;

			foreach ($result as $item) {
				$response[] = (int)$item[0];
			}

			return $response;

		}

		public static function disable() {

			self::$db->query('UPDATE ' . DB_PREFIX . 'category SET status = 0 WHERE uid <> \'\'');

		}

		public static function truncate() {

			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category_description');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'category_path');
			self::$db->query('DELETE FROM ' . DB_PREFIX . 'seo_url WHERE `push` LIKE \'route=product/category&path=%\'');

		}

	}
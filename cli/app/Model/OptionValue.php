<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class OptionValue extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					ov.option_value_id `0`,
					ov.option_value_id `1`,
					MD5(ovd.name) `2`
				FROM
					' . DB_PREFIX . 'option_value ov
				INNER JOIN
					' . DB_PREFIX . 'option_value_description ovd ON (ov.option_value_id = ovd.option_value_id)
				WHERE
					ovd.language_id = ' . App::getConfig('config_language_id') . '
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'option_value
				SET
					option_id = ' . $data['option_id'] . '
			');

			$id = self::$db->getLastId();

			self::$db->query('
				INSERT INTO
					' . DB_PREFIX . 'option_value_description
				SET
					option_value_id	= ' . $id . ',
					language_id		= ' . App::getConfig('config_language_id') . ',
					option_id		= ' . $data['option_id'] . ',
					`name`			= \'' . $data['name'] . '\'
			');

			return $id;

		}

		public static function truncate (): void {

			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'option_value');
			self::$db->query('TRUNCATE TABLE ' . DB_PREFIX . 'option_value_description');

		}

	}
<?php

	namespace App\Model;

	use App\App,
		App\Model;

	final class Returns extends Model {

		/**
		 * @return array
		 */
		public static function getAll (): array {

			return self::$db->query('
				SELECT
					return_id `0`,
					uid `1`,
					return_id `2`
				FROM
					`' . DB_PREFIX . 'return`
			')->rows;

		}

		/**
		 * @param array $data
		 *
		 * @return int
		 */
		public static function insert (array $data): int {

			self::$db->query('
				INSERT INTO
					`' . DB_PREFIX . 'return`
				SET
					deleted				= ' . $data['delete'] . ',
					uid					= \'' . self::$db->escape($data['id']) . '\',
					shipment_uid		= \'' . self::$db->escape($data['shipment_uid']) . '\',
					customer			= \'' . self::$db->escape($data['customer']) . '\',
					customer_id			= ' . $data['customer_id'] . ',
					products			= \'' . self::$db->escape(json_encode($data['products'], 256)) . '\',
					return_status_id	= ' . $data['return_status_id'] . ',
					comment				= \'' . self::$db->escape($data['comment']) . '\',
					date_added			= \'' . self::$db->escape($data['date']) . '\'
			');

			return self::$db->getLastId();

		}

		/**
		 * @param int $id
		 * @param array $data
		 *
		 * @return int
		 */
		public static function update (int $id, array $data): int {

			self::$db->query('
				UPDATE
					`' . DB_PREFIX . 'return`
				SET
					deleted				= ' . $data['delete'] . ',
					uid					= \'' . self::$db->escape($data['id']) . '\',
					shipment_uid		= \'' . self::$db->escape($data['shipment_uid']) . '\',
					customer			= \'' . self::$db->escape($data['customer']) . '\',
					customer_id			= ' . $data['customer_id'] . ',
					products			= \'' . self::$db->escape(json_encode($data['products'], 256)) . '\',
					return_status_id	= ' . $data['return_status_id'] . ',
					comment				= \'' . self::$db->escape($data['comment']) . '\',
					date_added			= \'' . self::$db->escape($data['date']) . '\'
				WHERE
					return_id = ' . $id . '
			');

			return $id;

		}

		/**
		 * @param int $id
		 *
		 * @return bool
		 */
		public static function cancel (int $id): bool {

			self::$db->query('
				UPDATE
					`' . DB_PREFIX . 'return`
				SET
					return_status_id = ' . App::getConfig('config_return_canceled_status_id') . ',
					deleted = 1
				WHERE
					return_id = ' . $id
			);

			return true;

		}

		/**
		 * @param int $id
		 *
		 * @return bool
		 */
		public static function delete (int $id): bool {

			self::$db->query('DELETE FROM `' . DB_PREFIX . 'return` WHERE return_id = ' . $id);

			return true;

		}

	}
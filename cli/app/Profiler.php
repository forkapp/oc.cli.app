<?php

	namespace App;

	final class Profiler {

		private static $start_time, $start_memory;

		public static function initialize () {

			self::$start_time = microtime(true);
			self::$start_memory = memory_get_usage(true);

		}

		public static function printInfo () {

			echo self::getInfo();

		}

		public static function getInfo (): string {

			return 'Execute time: [' . self::convertMicrotime(microtime(true) - self::$start_time) . ']'
				. "\n" . 'Memory usage: ['
				. self::convertMemory(memory_get_usage(true) - self::$start_memory) . ']' . "\n";

		}

		public static function convertMicrotime ($mt) {

			$micro = sprintf('%06d', ($mt - floor($mt)) * 1000000);
			$micro = new \DateTime(date('H:i:s.' . $micro, $mt));

			return $micro->format('i:s:v');

		}

		public static function convertMemory ($size) {

			$unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];

			return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];

		}

		public static function dd () {

			echo '<pre>';
			var_dump(func_get_args());
			echo '</pre>';
			exit;

		}

	}
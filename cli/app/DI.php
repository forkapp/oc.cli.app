<?php

	namespace App;

	final class DI {

		public static $data = [];

		public static function setPublic($key, $value) {

			self::$data[$key] = $value;

		}

		public static function getPublic($key) {

			return self::$data[$key] ?? null;

		}

	}
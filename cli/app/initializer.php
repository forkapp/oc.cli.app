<?php

	namespace App;

	use App\Model\Config,
		App\DI as DI;
	use App\Model\Language;

	final class App {

		public static $action, $method, $parameters, $language_ids;

		protected static $config;

		/**
		 * @throws \ReflectionException
		 * @throws \Exception
		 */
		public static function initialize() {

			global $config;

			self::autoload();

			Profiler::initialize();

			self::$config = $config;

			self::$parameters = [
				'config_store_id'		=> 0,
				'config_language_id'	=> 2,
			];

			self::connect_to_db();
			self::prepare();
			self::prepare_active_languages();

			DI::setPublic('parameters',	self::$parameters);
			DI::setPublic('config',		self::prepare_config());

			self::$parameters = array_merge(self::$parameters, DI::getPublic('config'));

			self::dispatch();

			Profiler::printInfo();

		}

		private static function connect_to_db() {

			Model::setLink(
				new \DB\mPDO(
					DB_HOSTNAME,
					DB_USERNAME,
					DB_PASSWORD,
					DB_DATABASE,
					DB_PORT
				)
			);

		}

		/**
		 * @return bool
		 * @throws \ReflectionException
		 * @throws \Exception
		 */
		private static function dispatch() {

			if ($class = self::exists_route()) {

				$class = new \ReflectionClass($class);
				$instance = $class->newInstanceWithoutConstructor();

				$instance->initialize();

				if (!$class->hasMethod(self::$method)) {
					throw new \Exception('Method: ' . self::$method . ' not declared!');
				}

				$instance->{self::$method}();

				return true;

			}

			throw new \Exception('Not allowed action in route!');

		}

		private static function exists_route() {

			return self::$config['routes'][self::$action] ?? false;

		}

		/**
		 * @throws \Exception
		 */
		private static function prepare() {

			global $argc, $argv;

			$argv = array_slice($argv, 1);

			if ($argc <= 1) {
				throw new \Exception('Action not setted!');
			}

			self::$action = trim($argv[0]);

			if ($argc <= 2) {
				throw new \Exception('Method not setted!');
			}

			self::$method = trim($argv[1]);

			$argv = array_slice($argv, 2);

			foreach ($argv as $key => $val) {
				$val = explode(':', trim($val));
				self::$parameters[$val[0]] = $val[1];
			}

		}

		private static function autoload() {

			spl_autoload_register(function($class) {

				$prefix = 'App\\';

				$len = strlen($prefix);
				if (strncmp($prefix, $class, $len) !== 0) {
					return false;
				}

				$file = __DIR__ . '/' . str_replace('\\', '/', substr($class, $len)) . '.php';

				if (file_exists($file)) {
					include $file;
					return true;
				}

				throw new \Exception('Can\'t load ' . $class . "\n");

			});

		}

		/**
		 * @param $file
		 */
		public static function load ($file): bool {

			if (!is_array($file)) {

				if (file_exists($file)) {
					include_once $file;
					return true;
				}

				return false;

			}

			foreach ($file as $f) {

				if (file_exists($f)) {
					include_once $f;
					continue;
				}

				return false;

			}

			return true;

		}

		private static function prepare_active_languages() {

			$languages = [];

			foreach (Language::getAll() as $language) {
				$languages[$language['language_id']] = [
					'code'	=> $language['code'],
					'name'	=> $language['name'],
				];
			}

			DI::setPublic('languages', $languages);

			self::$language_ids = array_keys($languages);

		}

		public static function getLanguageIds() : array {

			return (array)self::$language_ids;

		}

		private static function prepare_config() : array {

			$result = [];

			foreach (Config::getAll() as $value) {
				$result[$value['key']] = !$value['serialized'] ? $value['value'] : @serialize($value['value']);
			}

			return $result;

		}

		public static function getConfig($key) {

			return self::$parameters[$key] ?? null;

		}

	}